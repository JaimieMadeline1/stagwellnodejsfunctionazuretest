- Make an api.
- Make an cosmosdb.
- Make an functions, and a create new storage bucket.
- Go into newly created Functions App, and create new functions, similar to the ones in repo, but with the newly created constants for database name and collection name.
- Connect functions to api and add the cors specifications.
- Change the url to point to the new api on the front end and the api credentials.

# API Setup
The Api Management is what the client side will be calling.
Creating a new API in azure may take from 30 to 60 minutes after choosing the name,
 so start with this first, and only afterwards plug in the functions and appropriate CORS.

- Go to 'API Management services'
- Click on Add, and plug in the appropriate details then click the create button.
name: hpsc-api-hp-sample-book
subscription: pay-as-you-go or whatever hp currently uses.
Make a new Resource Group: hpsc-hp-sample-book-res
Location: Us central
Organization Name: hpsc-api-hp-sample-book-org
Admin Email: Whatever is currently there
Pricing Tier: Basic (https://gyazo.com/9117d2cbf73ac2d67bc4e85ef101c0ba)

# Making the cosmosdb
- Go to 'Azure Cosmos DB'
- Click on Add, plug in the details and select review+create, then the create button.
Pick the same resource group as the api.
account name: hpsc-hp-sample-book-comos
Api: Core(SQL)
Location: Us central
Enable Geo-Redudency
Disable Multi-region writes
- After the Cosmos DB has been created, add a new database and collection, found under 'Data Explorer' tab.
hpsc-hp-sample-book-comos-database
hpsc-hp-sample-book-comos-collection
Storage Capacity: unlimited
Partition key: /hpscHpSampleBookComosCollectionPartKey
Throughput: 400(the minimum allowed)

# Making cloud functions
- Go to 'App Services'
- Click Add, then find 'Function App', click create.
hpsc-hp-sample-book-function
Resource use existing, the same one that's used by the api.
Consumption Plan
Location: Us central
Runtime stack: javascript
create new storage: hpschpsamplebookblob
Application Insights(Optional, could put to 'off' as don't need it currently.)

# Add a container inside the Blob Storage
Go to the new blob created when creating the cloud functions, 'hpschpsamplebookblob' or whatever name you chose.
Under Blob Service section click on 'Blobs' and add a container.

# Integrating functions into newly created Function App Part 1
- Go to the function app that you just created.

- Click on 'hpsc-hp-sample-book-function - HttpTrigger-HpSampleBook-GetImage'
- inside the sub-menu 'Integrate', under 'Inputs' find 'Azure Blob Storage',
 find Storage account connection, there will be a 'new' button in blue to the 
 right of the input box, upon clicking on it select the blob created 'hpschpsamplebookblob', then hit save button.

- Click on 'hpsc-hp-sample-book-function - HttpTrigger-HpSampleBook-PostImage'
- inside the sub-menu 'Integrate', under 'Outputs' find 'Azure Blob Storage',
 find Storage account connection, there will be a 'new' button in blue to the 
 right of the input box, upon clicking on it select the blob created 'hpschpsamplebookblob', then hit save button.

- Click on 'hpsc-hp-sample-book-function - HttpTrigger-HpSampleBook-AddSubmittedInfo'
- inside the sub-menu 'Integrate', under 'Outputs' find 'Azure Cosmos DB',
 find Azure Cosmos DB account connection, there will be a 'new' button in blue to the 
 right of the input box, upon clicking on it select the cosmos storage that you created, then hit save button.

- Click on 'hpsc-hp-sample-book-function - HttpTrigger-HpSampleBook-ReadSubmittedInfo'
- inside the sub-menu 'Integrate', under 'Inputs' find 'Azure Cosmos DB',
 find Azure Cosmos DB account connection, there will be a 'new' button in blue to the 
 right of the input box, upon clicking on it select the cosmos storage that you created, then hit save button.

# Integrating functions into newly created Function App Part 2
- For the two cosmosdb functions, the appropriate variables must be used inside their function.json files.
- Click on each of the functions, and go to function.json file, and change the values for databaseName and the collectionName that you created accordingly for the cosmodDB database.

# Integrating functions into newly created Function App Part 3
- For the two blob functions, update their function.json file with the correct path, changing the container name to the one that you made for the blob storage.

# Integrating functions into newly created Function App Part 4 - Install binding extension
- Under the Integrate section of HttpTrigger-HpSampleBook-GetImage, click New Input, and select Blob Storage, it will prompt you to install an extension, click to install it, then cancel/remove the selected input, because its not actually needed, this is just the fastest way to install the extension.

- Under the Integrate section of  HttpTrigger-HpSampleBook-AddSubmittedInfo, click New Input, and select Azure Cosmos Database, it will prompt you to install an extension, click to install it, then cancel/remove the selected input because its not actually needed, this is just the fastest way to install the extension.

# API management, adding the cloud functions to the Api.
- Go to your api 'hpsc-api-hp-sample-book', go to the api tab, found on the left.
- Click 'Add API', browse, under Function App (Configure required settings) and find the function app that you created, wait for the functions to load, the select all.
- For the 'Products' select 'Unlimited' option.

# API management Cors
- In the APIs section of the selected API Management Service, click from the left hand side on the created api, under 'Inbound processing' or 'Outbound processing' click on icon right of 'Policies' to go into xml editor, copy the CORS section from below.

<policies>
    <inbound>
        <base />
        <cors>
            <allowed-origins>
                <origin>https://cwd.pmxagency.com</origin>
            </allowed-origins>
            <allowed-methods>
                <method>GET</method>
                <method>POST</method>
            </allowed-methods>
            <allowed-headers>
                <header>Ocp-Apim-Subscription-Key</header>
                <header>Cache-Control</header>
                <header>content-type</header>
                <header>accept</header>
                <header>Authorization</header>
                <header>X-Requested-With</header>
            </allowed-headers>
            <expose-headers>
                <header>*</header>
            </expose-headers>
        </cors>
    </inbound>
    <backend>
        <base />
    </backend>
    <outbound>
        <base />
    </outbound>
    <on-error>
        <base />
    </on-error>
</policies>

# On the client side change to use the Api's product key and the new url.
The new api url can be found when going back to the selected API Management Service, it should be under 'Overview', under 'Gateway URL', 'https://hpsc-api-hp-sample-book.azure-api.net'

The new Ocp-Apim-Subscription-Key can be found under the Overview Tab -> Developer Portal -> Products -> Unlimited -> 'Unlimited (default)' -> will be under 'Your subscriptions' under the unlimited, click show to the right of 'Primary key'.

Change the client side accordingly to use the url and the key when making requests to the api.

# Testing getting data and getting images from back-end.

// Get all contents from cosmos db.
// axios({
// 	method: "GET",
// 	url:"https://hpsc-api-hp-sample-book.azure-api.net/hpsc-hp-sample-book-function/HttpTrigger-HpSampleBook-ReadSubmittedInfo",
// 	headers: {
// 		"Ocp-Apim-Subscription-Key": "ba288f1765b3497485e5089f14be8373",
// 		"Cache-Control": "no-cache"
// 	}
// 	})
// 	.then(function(dataResponse) {
// 	// handle success
// 	  if (dataResponse && dataResponse.data) {
// 			console.log(dataResponse.data);
// 	  }
// 	})
// 	.catch(function(error) {
// 		// handle error
// 		console.log(error);
// 	})


// Get contents of a specific image from azure blob storage.
// getSimpleImageRequest = () => {
// 	axios({
// 	  method: "GET",
// 	  url: "https://hpsc-api-hp-sample-book.azure-api.net/hpsc-hp-sample-book-function/HttpTrigger-HpSampleBook-GetImage",
// 	  params: {
// 		name: "testImageUpload2.jpg"
// 	  },
// 	  headers: {
// 		"Ocp-Apim-Subscription-Key": "ba288f1765b3497485e5089f14be8373",
// 		"Cache-Control": "no-cache"
// 	  }
// 	})
// 	  .then((dataResponse) => {
// 		// handle success
// 		if (dataResponse && dataResponse.data) {
// 		  var newTemp = dataResponse.data.imageData.thumb;
// 		  this.setState({ imageResource: newTemp });
// 		}
// 	  })
// 	  .catch(function(error) {
// 		// handle error
// 		console.log(error);
// 	  });
//   };