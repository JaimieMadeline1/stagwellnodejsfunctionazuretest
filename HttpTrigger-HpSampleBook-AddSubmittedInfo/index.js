module.exports = async function (context, req) {

    // We need both name and task parameters.
    if (req.body && req.body.user && req.body.data && req.body.currentSampleBookSection) {
        // Set the output binding data from the query object.
        var newDataObj = {
            user: req.body.user,
            data: req.body.data,
            currentSampleBookSection: req.body.currentSampleBookSection
        }
        context.bindings.outputDocument = newDataObj;

        // Success.
        context.res = {
            status: 200,
            body: "Uploaded User Data."
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Make sure the correct body parameters are given."
        };
    }
};