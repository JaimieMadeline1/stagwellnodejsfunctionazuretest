module.exports = async function (context, req) {

    if (context.bindings.myInputBlob && req.query.blobfilename) {
        context.res = {
            status: 200,
            body: context.bindings.myInputBlob
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Couldn't read server data, check if connection variables are correct. Please pass a blobfilename name of the iamge that you are requesting."
        };
    }
};