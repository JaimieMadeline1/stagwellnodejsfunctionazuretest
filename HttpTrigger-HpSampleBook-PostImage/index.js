module.exports = async function (context, req) {
    
    if (req.body) {
        context.bindings.outputBlob = req.body;
        context.res = {
            status: 200,
            body: "Uploaded Image!"
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Please pass a blobfilename name and the object inside the body."
        };
    }
};