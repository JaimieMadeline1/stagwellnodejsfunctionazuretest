module.exports = async function (context, req) {

    if (context.bindings.documents) {

        // Success.
        context.res = {
            status: 200,
            body: context.bindings.documents
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Couldn't read server data, check if connection variables are correct."
        };
    }
};